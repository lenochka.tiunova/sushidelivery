<div class="container-footer">
	<div class="footer-row-1">
		<div class = "footer-col footer-col-1-1" id = "footer-menu-1">
			<ul class="footer-list-link">
				<li><a href="/web/index.php#rolly" class="footer-elem">Роллы</a></li>
				<li><a href="/web/index.php#mini-rolly" class="footer-elem">Мини роллы</a></li>
				<li><a href="/web/index.php#goryachie-rolly" class="footer-elem">Горячие роллы</a></li>
				<li><a href="/web/index.php#sushi" class="footer-elem">Суши</a></li>
				<li><a href="/web/index.php#sety" class="footer-elem">Сеты</a></li>
			</ul>
		</div>
		<div class = "footer-col footer-col-1-2" id = "footer-menu-2">
			<ul class="footer-list-link">
				<li><a href="/web/index.php#vok" class="footer-elem">ВОК</a></li>
				<li><a href="/web/index.php#burgery" class="footer-elem">Бургеры</a></li>
				<li><a href="/web/index.php#zakuski" class="footer-elem">Закуски</a></li>
				<li><a href="/web/index.php#deserty" class="footer-elem">Десерты</a></li>
				<li><a href="/web/index.php#napitki" class="footer-elem">Напитки</a></li>
			</ul>
		</div>
		<div class = "footer-col footer-col-1-3">
			<ul class="footer-list-link">
				<li>Мистер крабс</li>
				<li><a href="delivery.php" class="footer-elem">Доставка</a></li>
				<li><a href="index.php" class="footer-elem">Контакты</a></li>
			</ul>
		</div>
		<div class = "footer-col footer-col-1-4">
			<p class="footer-text">Адрес:</p>
			<ul class="footer-list">
				<li>г. Омск</li>
				<li>ул. 21-амурская, 16</li>
			</ul>
		</div>
		<div class = "footer-col footer-col-1-5">
			<p class="footer-text">Режим работы:</p>
			<ul class="footer-list">
				<li>Ежедневно</li>
				<li>с 10:00 до 23:00</li>
				<li class="footer-tel">Телефон: <a href="tel:+73812502525" class="footer-elem">50-25-25</a></li>
			</ul>
		</div>
		<div class = "footer-col footer-col-1-6">
			<a href="https://www.instagram.com/krabs_sushi/" class="footer-link"><img src="images/inst.svg" class="footer-img"></a>
			<a href="https://vk.com/mr.krabs2116" class="footer-link"><img src="images/vk.svg" class="footer-img"></a>
		</div>
	</div>
	<hr class="footer-line">
	<div class="footer-row-2">
		<div class = "footer-col footer-col-1">
			<p class="footer-header">© Мистер крабс</p>
		</div>
		<div class = "footer-col footer-col-2">
			<a href="confidentiality.php" class="footer-elem">Политика конфиденциальности</a>
		</div>
		<div class = "footer-col footer-col-3">
			<a href="https://laikaweb.ru/" class="footer-elem">Разработка сайта веб - студия LAIKA</a>
		</div>
	</div>
</div>