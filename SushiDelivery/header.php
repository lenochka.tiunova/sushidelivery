<div class="header-row-1">
	<div class="col-1 header-block">
		<p class="header-text">Адрес:</p>
		<ul class="header-list">
			<li>г. Омск</li>
			<li>ул. 21-амурская, 16</li>
		</ul>
	</div>
	<div class="col-2 header-block">
		<p class="header-text">Режим работы:</p>
		<ul class="header-list">
			<li>Ежедневно</li>
			<li>с 10:00 до 23:00</li>
		</ul>
	</div>
	<div class="col-3">
		<a href="index.php" class="custom-logo-link"><img width="381" height="85" src="images/logo.png" class="custom-logo" alt="Доставка в Омске, Мистер-Крабс, 21 Амурская, 16. Работаем с 10:00 до 23:00" /></a>
	</div>
	<div class="span-blok">
		<div class="col-4">
			<a class="header-tel" href="tel:+73812502525">50-25-25</a>
		</div>
		<div class="col-5">
			<a href="#" title="Оформить заказ" class="cart"><img src="images/cart.svg"  class="img-cart" alt="Корзина">0</a>
		</div>
		<div class="col-6">
			<?php
            //Проверяем авторизован ли пользователь
            if(!isset($_SESSION['email']) && !isset($_SESSION['password'])){
                // если нет, то выводим блок с ссылками на страницу регистрации и авторизации
            ?>
				<a href="http://localhost/web/form_auth.php" title="Вход и регистрация" class="login">
					<img src="images/login.png" class="img-login" alt="Вход">
					<span class="login-text">Войти</span>
				</a>
            <?php
                } else {
                    //Если пользователь авторизован, то выводим ссылку Выход
            ?> 
				<a href="http://localhost/web/logout.php" title="Выход из аккаунта" class="login">
					<img src="images/login.png" class="img-login" alt="Выход">
					<span class="login-text">Выйти</span>
				</a>
            <?php
                }
            ?>
		</div>
	</div>
</div>