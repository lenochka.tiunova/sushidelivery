<?php
    //Запускаем сессию
    session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Доставка и оплата | Мистер Крабс – Доставка суши и роллов в Омске</title>
	<link rel="icon" type="image" href="images/icon.png">
	<link rel="stylesheet" type="text/css" href="styles/base/body.css">
	<link rel="stylesheet" type="text/css" href="styles/base/header.css">
	<link rel="stylesheet" type="text/css" href="styles/base/header-row.css">
	<link rel="stylesheet" type="text/css" href="styles/base/footer.css">
	<link rel="stylesheet" type="text/css" href="styles/components/delivery.css">
	<link rel="stylesheet" type="text/css" href="styles/fonts/fonts.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<header>
	<div class="container">
	<?php
		require_once 'header.php';
		require_once 'header-row.php';
	?>
	</div>
</header>
<main class="main-delivery">
	<div class="info-delivery">
		<h1 class="section_header">Доставка и оплата</h1>
		<div class="main-info-delivery">
			<p><strong>Доставка</strong></p>
			<p>Доставка осуществляется по Омску, время доставки до 60 мин.</p>
			<p>Режим работы: ежедневно с 10:00 до 23:00</p>
			<p>Телефон службы доставки: 50-25-25</p>
			<p><strong>Оплата</strong></p>
			<p>Оплатить заказ можно картой, либо наличными курьеру</p>
			<p><strong>Самовывоз</strong></p>
			<p>На сайте можно оформить заказ на самовывоз и забрать его по адресу: г. Омск, ул.&nbsp; ул, 21 Амурская, 16</p>
		</div>
	</div>
</main>
<footer>
	<?php
		require_once 'footer.php';
	?>
</footer>
</body>
</html>