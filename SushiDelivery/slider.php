<div class="slider">
	<div class="slide">
		<img src="images/photo_slider_1.jpg" class="img-fluid" alt="Доставка суши">
		<div class="slider_text_wrapper text-center">
			<p class="text-uppercase header_text_slide">Доставка <span class="red_text">суши</span></p>
			<p class="desc-slide">Бесплатная доставка по всему городу от 600 руб.</p>
			<a href="#rolly" class="slider__lnk"><button class="slider__btn">Заказать</button></a>
		</div>
	</div>
</div>