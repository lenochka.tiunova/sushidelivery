<?php
    //Запускаем сессию
    session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="HandheldFriendly" content="true">
	<title>Мистер Крабс - Доставка суши и &nbsp;&nbsp;&nbsp;роллов в Омске</title>
	<link rel="icon" type="image" href="images/icon.png">
	<link rel="stylesheet" type="text/css" href="styles/base/body.css">
	<link rel="stylesheet" type="text/css" href="styles/base/header.css">
	<link rel="stylesheet" type="text/css" href="styles/base/header-row.css">
	<link rel="stylesheet" type="text/css" href="styles/base/footer.css">
	<link rel="stylesheet" type="text/css" href="styles/base/main.css">
	<link rel="stylesheet" type="text/css" href="styles/components/basket.css">
	<link rel="stylesheet" type="text/css" href="styles/components/slider.css">
	<link rel="stylesheet" type="text/css" href="styles/fonts/fonts.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<header>
	<div class="container">
	<?php
		require_once 'header.php';
		require_once 'header-row.php';
	?>
	</div>
</header>
<main>
		<?php
			require_once 'slider.php';
			echo "<div class='main-container'>";
			require_once 'database/work_data.php';
			echo "</div>";
			require_once 'fixed-cart.php';
		?>
</main>
<footer>
	<?php
		require_once 'footer.php';
	?>
</footer>
<script src="scripts/script-basket.js"></script>
<script src="scripts/script-navigation.js"></script>
</body>
</html>
