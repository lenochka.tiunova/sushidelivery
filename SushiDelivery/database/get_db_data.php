<?php
/**
 * Get all available products.
 */
function getProducts($conn) {
	$query = "SELECT pwc.id as idMain, p.nameProduct, p.descriptionProduct, p.weight, p.price, c.category, c.id, p.image
	FROM productwithcategory pwc, category c, product p WHERE pwc.categoryid = c.id
	AND pwc.productid = p.id";

	$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
	$products = [];
	for ($products; $row = mysqli_fetch_assoc($result); $products[] = $row);
	
	return $products;
}