<?php
require_once 'get_db_data.php';
require_once 'db_connection.php';
$conn = OpenCon();
/**
 * Count unique categories.
 */ 
function getUniqueCategories($data) {
	$unique_categories = [];
	foreach ($data as $key => $value) {
		if (in_array($value['category'], $unique_categories)) {
			continue;
		}
		else {
			$unique_categories[$value['id']] = $value['category'];		
		}
	}

	return $unique_categories;
}

$products = getProducts($conn);

$unique_categories = getUniqueCategories($products);

function getProductsCategory($data, $category) {
	$product_categories = [];
	foreach ($data as $key => $value) {
		if ($value['category'] === $category) {
			$product_categories[] = $value;
		}
	}
	return $product_categories;
}

function translit($str) {
    $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
    $lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
    $str = str_replace($rus, $lat, $str);
    return strtolower(str_replace(" ", "-", $str));
  }

function renderProducts($unique_categories, $products) {
	foreach ($unique_categories as $category) {
		echo "<div class = 'category' id = '" . translit($category) .
			"'><h1 class = 'mainHeader'>" . $category . "</h1>
			<div class = 'products'>";
		$productsCategory = getProductsCategory($products, $category);
		foreach ($productsCategory as $product) {
			echo "<div class='product'>
					<img class = 'imgProduct' src='" . $product['image'] ."' alt='" . $product['nameProduct'] . "' class='photo_product'>
					<h2 class = 'mainHeader2'>" . $product['nameProduct'] . "</h2>
					<p class = 'productDescription'>" . $product['descriptionProduct'] . "</p>
					<p class = 'productWeight'>" . $product['weight'];
					if($product['weight'] != null){
							echo(" г");
						}
					echo "</p><p class = 'productPrice'>" . $product['price'] . ' ₽' . "</p>
				</div>";
		}
		echo "</div></div>";
	}
}

renderProducts($unique_categories, $products);

CloseCon($conn);