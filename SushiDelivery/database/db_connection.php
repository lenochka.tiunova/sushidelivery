<?php
function OpenCon() {
	$dbhost = "localhost";
	$dbuser = "root";
	$dbpass = "";
	$db = "sushidelivery";
	$conn = new mysqli($dbhost, $dbuser, $dbpass,$db) or die("Connect failed: %s\n". $conn -> error);

	 mysqli_query($conn, "SET NAMES 'utf8'");

	return $conn;
}

$address_site = "http://localhost/web";

function CloseCon($conn) {
	$conn -> close();
}