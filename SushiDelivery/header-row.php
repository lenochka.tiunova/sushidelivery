<div class="header-row-2">
	<div id="header-menu">
		<ul class="nav-header">
			<li class="nav-item"><a class="nav-link" href="/web/index.php#rolly">Роллы</a></li>
			<li class="nav-item"><a class="nav-link" href="/web/index.php#mini-rolly">Мини роллы</a></li>
			<li class="nav-item"><a class="nav-link" href="/web/index.php#goryachie-rolly">Горячие роллы</a></li>
			<li class="nav-item"><a class="nav-link" href="/web/index.php#sushi">Суши</a></li>
			<li class="nav-item"><a class="nav-link" href="/web/index.php#sety">Сеты</a></li>
			<li class="nav-item"><a class="nav-link" href="/web/index.php#vok">ВОК</a></li>
			<li class="nav-item"><a class="nav-link" href="/web/index.php#burgery">Бургеры</a></li>
			<li class="nav-item"><a class="nav-link" href="/web/index.php#dopolneniya">Доп.меню</a></li>
			<li class="nav-item"><a class="nav-link" href="/web/index.php#zakuski">Закуски</a></li>
			<li class="nav-item"><a class="nav-link" href="/web/index.php#deserty">Десерты</a></li>
			<li class="nav-item"><a class="nav-link" href="/web/index.php#napitki">Напитки</a></li>
		</ul>
	</div>
</div>
<script src="scripts/script-header.js"></script>