<?php
    //Запускаем сессию
    session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Авторизация | Мистер Крабс – Доставка суши и роллов в Омске</title>
    <link rel="stylesheet" type="text/css" href="styles/base/body.css">
    <link rel="stylesheet" type="text/css" href="styles/base/header.css">
    <link rel="stylesheet" type="text/css" href="styles/base/footer.css">
    <link rel="stylesheet" type="text/css" href="styles/components/auth.css">
    <link rel="icon" type="image" href="images/icon.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="scripts/script-auth.js"></script>
</head>
<body>
<header>
    <div class="container">
    <?php
        require_once 'header.php';
    ?>
    </div>
</header>
<main>
    <div class="main-blok">
    <div class="block_for_messages">
    <?php
 
        if(isset($_SESSION["error_messages"]) && !empty($_SESSION["error_messages"])){
            echo $_SESSION["error_messages"];
 
            //Уничтожаем чтобы не появилось заново при обновлении страницы
            unset($_SESSION["error_messages"]);
        }
 
        if(isset($_SESSION["success_messages"]) && !empty($_SESSION["success_messages"])){
            echo $_SESSION["success_messages"];
             
            //Уничтожаем чтобы не появилось заново при обновлении страницы
            unset($_SESSION["success_messages"]);
        }
    ?>
</div>
 
<?php
    //Проверяем, если пользователь не авторизован, то выводим форму авторизации, 
    //иначе выводим сообщение о том, что он уже авторизован
    if(!isset($_SESSION["email"]) && !isset($_SESSION["password"])){
?>    
    <div id="form_auth">
        <p class="main-header">Авторизация</p>
        <form action="http://localhost/web/auth.php" method="POST" name="form_auth" class="form">
            <table class="main-table">
                <tbody><tr>
                    <td>
                        <input type="email" name="email" placeholder="Email" required="required" class="text-field"><br>
                        <span id="valid_email_message" class="message_error"></span>
                    </td>
                </tr>
          
                <tr>
                    <td>
                        <input type="password" name="password" placeholder="Пароль" required="required" class="text-field"><br>
                        <span id="valid_password_message" class="message_error"></span>
                    </td>
                </tr>
                 
                <tr>
                    <td>
                        <p>
                            <img src="http://localhost/web/captcha.php" alt="Изображение капчи" class="captcha-img" /> <br>
                            <input type="text" name="captcha" placeholder="Введите код с картинки" class="text-field">
                        </p>
                    </td>
                </tr>
 
                <tr>
                    <td colspan="2">
                        <input type="submit" name="btn_submit_auth" class="btn-come-in" value="Войти">
                    </td>
                </tr>

                <tr>
                    <td colspan="3">
                        <a href="http://localhost/web/form_register.php" class="link-reg">Регистрация</a>
                    </td>
                </tr>
            </tbody></table>
        </form>
    </div>
<?php
    }else{
?>
    <div id="authorized">
        <h2>Вы уже авторизованы</h2>
    </div>
<?php
    }
?>
</div>
</main>
<footer>
<?php
    require_once 'footer.php';
?>
</footer>
</body>
</html>