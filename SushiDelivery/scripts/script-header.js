const sticky_menu = $('#header-menu');
const menu_top = sticky_menu.offset().top;

const isHeaderSticky = () => {
    const offset = $(this).scrollTop();
    if(menu_top < offset) {
        sticky_menu.addClass('header_fixed');
    } 
    else {
        sticky_menu.removeClass('header_fixed');
    }
}

$(window).scroll(() => {
    isHeaderSticky();
});

$(document).ready(() => {
    isHeaderSticky();
});