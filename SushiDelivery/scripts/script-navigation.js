$(document).ready(function() {
    $("#header-menu, #footer-menu-1, #footer-menu-2").on("click","a", function (event) {
        event.preventDefault();
        var id  = '#' + $(this).attr('href').split('#')[1];
            top = $(id).offset().top - $("#header-menu").outerHeight();
         $('body, html').animate({scrollTop: top}, 1000);
         window.location.href = $(this).attr('href');
    });

    const location = window.location;
    if (location.hash) {
        var top = $(location.hash).offset().top - $("#header-menu").outerHeight();
            $('body, html').animate({scrollTop: top}, 1000);
    }
});

function isVisible(target) {
if (!target) {
     return;
}
const target_position = {
  top: window.pageYOffset + target.getBoundingClientRect().top + 400,
  bottom: window.pageYOffset + target.getBoundingClientRect().bottom - 400
};
const window_position = {
  top: window.pageYOffset,
  bottom: window.pageYOffset + document.documentElement.clientHeight
};
return target_position.bottom > window_position.top && target_position.top < window_position.bottom;
}

window.addEventListener('scroll', () => {
const header_meny = $('#header-menu');

for (const item of header_meny.find('.nav-link')) {
  const target = $(`#${item.href.split('#')[1]}`)[0];
  $(item).toggleClass('menu-item-active', isVisible(target));
}
});