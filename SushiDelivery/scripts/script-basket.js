const basket_fix = $('.fixed-cart');
const img_offset = $('.img-fluid').offset().top + $('.img-fluid').height()/4;

$(window).scroll(function () {
    const offset = $(this).scrollTop();
    if(img_offset < offset) {
        basket_fix.addClass('basket_display_block');
        basket_fix.removeClass('basket_display_none');
    } else {
        if (basket_fix.css('visibility') != 'hidden') {
            basket_fix.addClass('basket_display_none');
            basket_fix.removeClass('basket_display_block');
        }
    }
});