<?php
    //Запускаем сессию
    session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Регистрация | Мистер Крабс – Доставка суши и роллов в Омске</title>
    <link rel="stylesheet" type="text/css" href="styles/base/body.css">
    <link rel="stylesheet" type="text/css" href="styles/base/header.css">
    <link rel="stylesheet" type="text/css" href="styles/base/footer.css">
    <link rel="stylesheet" type="text/css" href="styles/components/auth.css">
    <link rel="icon" type="image" href="images/icon.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="scripts/script-auth.js"></script>
</head>
<body>
<header>
    <div class="container">
    <?php
        require_once 'header.php';
    ?>
    </div>
</header>
<main>
<div class="main-blok">
    <!-- Блок для вывода сообщений -->
    <div class="block_for_messages">
        <?php
            //Если в сессии существуют сообщения об ошибках, то выводим их
            if(isset($_SESSION["error_messages"]) && !empty($_SESSION["error_messages"])){
                echo $_SESSION["error_messages"];
     
                //Уничтожаем чтобы не выводились заново при обновлении страницы
                unset($_SESSION["error_messages"]);
            }
     
            //Если в сессии существуют радостные сообщения, то выводим их
            if(isset($_SESSION["success_messages"]) && !empty($_SESSION["success_messages"])){
                echo $_SESSION["success_messages"];
                 
                //Уничтожаем чтобы не выводились заново при обновлении страницы
                unset($_SESSION["success_messages"]);
            }
        ?>
    </div>
     
    <?php
        //Проверяем, если пользователь не авторизован, то выводим форму регистрации, 
        //иначе выводим сообщение о том, что он уже зарегистрирован
        if(!isset($_SESSION["email"]) && !isset($_SESSION["password"])){
    ?>
            <div id="form_register">
                <p class="main-header">Регистрация</p>
                
                <form action="http://localhost/web/register.php" method="post" name="form_register" class="form">
                    <table class="main-table">
                        <tbody><tr>
                            <td>
                                <input type="text" name="first_name" placeholder="Имя" required="required" class="text-field">
                            </td>
                        </tr>
     
                        <tr>
                            <td>
                                <input type="text" name="last_name" placeholder="Фамилия" required="required" class="text-field">
                            </td>
                        </tr>
                  
                        <tr>
                            <td>
                                <input type="email" name="email" placeholder="Email" required="required" class="text-field"><br>
                                <span id="valid_email_message" class="message_error"></span>
                            </td>
                        </tr>
                  
                        <tr>
                            <td>
                                <input type="password" name="password" placeholder="Пароль" required="required" class="text-field"><br>
                                <span id="valid_password_message" class="message_error"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <img src="http://localhost/web/captcha.php" alt="Изображение капчи" class="captcha-img" /> <br>
                                    <input type="text" name="captcha" placeholder="Введите код с картинки" class="text-field">
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" name="btn_submit_register" value="Зарегистрироваться" class="btn-come-in">
                            </td>
                        </tr>
                    </tbody></table>
                </form>
            </div>
    <?php
        }else{
    ?>
        <div id="authorized">
                <h2>Вы уже зарегистрированы</h2>
        </div>
    <?php
    }
    ?>
</div>
</main> 
<footer>
<?php
    require_once 'footer.php';
?>
</footer>
</body>
</html>